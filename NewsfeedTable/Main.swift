//
//  Main.swift
//  NewsfeedTable
//
//  Created by Dominik Babic on 06/04/2017.
//  Copyright © 2017 CodeCons. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper
import RealmSwift

class Main: Object, Mappable {

    dynamic var temp = 0.0
    dynamic var pressure = 0.0
    dynamic var humidity = 0.0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        temp        <- map["temp"]
        pressure    <- map["pressure"]
        humidity    <- map["humidity"]
    }
}
