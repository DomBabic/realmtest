//
//  ViewController.swift
//  NewsfeedTable
//
//  Created by Dominik Babic on 06/04/2017.
//  Copyright © 2017 CodeCons. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private var requestHandler = RequestHandler()
    private var reuseIdentifier = "cell"
    private var tableItems = try! Realm().objects(DataModel.self)
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var locationTextField: UITextField!
    
    @IBAction func fetchButton(_ sender: UIButton) {
        if locationTextField.text != "" {
            requestHandler.getRequest(locationTextField.text!, tableView)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableItems.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CustomCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! CustomCell
        
        cell.cityLabel?.text = self.tableItems[indexPath.row].name
        cell.tempLabel?.text = "\(self.tableItems[indexPath.row].main!.temp)"
        cell.presLabel?.text = "\(tableItems[indexPath.row].main!.pressure)"
        cell.humLabel?.text = "\(tableItems[indexPath.row].main!.humidity)"
        cell.lonLabel?.text = "\(tableItems[indexPath.row].coord!.lon)"
        cell.latLabel?.text = "\(tableItems[indexPath.row].coord!.lat)"
        cell.windLabel?.text = "\(tableItems[indexPath.row].wind!.speed)"
        
        print()
        
        return cell
    }
    
}
