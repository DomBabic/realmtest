//
//  RequestHandler.swift
//  NewsfeedTable
//
//  Created by Dominik Babic on 06/04/2017.
//  Copyright © 2017 CodeCons. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

class RequestHandler {

    private var API_KEY = "ed916196c822944545567e785f44d10f"
    private var baseURL = "http://api.openweathermap.org/data/2.5/weather"
    private var origin: UITableView?
    
    func getRequest(_ location: String, _ source: UITableView?) {
        origin = source
        Alamofire.request(baseURL, method: .get, parameters: ["q": location, "appid": API_KEY])
            .responseObject { (response: DataResponse<DataModel>) in
                if let data = response.result.value {
                    do {
                        let realm = try Realm()
                        try realm.write {
                            realm.add(data, update: true)
                            self.origin!.reloadData()
                        }
                    } catch let err as NSError {
                        print("Error with Realm:\(err.localizedDescription)")
                    }
                } else {
                    print("failed")
                }
            }
    }
}
