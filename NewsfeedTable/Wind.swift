//
//  Wind.swift
//  NewsfeedTable
//
//  Created by Dominik Babic on 06/04/2017.
//  Copyright © 2017 CodeCons. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper
import RealmSwift

class Wind: Object, Mappable {
    
    dynamic var speed = 0.0
    dynamic var deg = 0.0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        speed   <- map["speed"]
        deg     <- map["deg"]
    }
}
