//
//  Coord.swift
//  NewsfeedTable
//
//  Created by Dominik Babic on 06/04/2017.
//  Copyright © 2017 CodeCons. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper
import RealmSwift

class Coord: Object, Mappable {

    dynamic var lon = 0.0
    dynamic var lat = 0.0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        lon <- map["lon"]
        lat <- map["lat"]
    }
}
