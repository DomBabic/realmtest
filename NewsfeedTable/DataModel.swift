//
//  DataModel.swift
//  NewsfeedTable
//
//  Created by Dominik Babic on 06/04/2017.
//  Copyright © 2017 CodeCons. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import ObjectMapper
import RealmSwift

class DataModel: Object, Mappable {
    
    dynamic var name: String?
    dynamic var coord: Coord?
    dynamic var main: Main?
    dynamic var wind: Wind?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name    <- map["name"]
        coord   <- map["coord"]
        main    <- map["main"]
        wind    <- map["wind"]
    }
    
    override static func primaryKey() -> String? {
        return "name"
    }

}
